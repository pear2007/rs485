using System.IO.Ports;

namespace cs01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                textBox1.Text = "测试串口";
                label1.Text = "打开";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x00, 0x00, 0x01, 0x48, 0x0A };
                //4号关闭    01 06 00 03 00 01 B8 0A 
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                label1.Text = "关闭";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x00, 0x00, 0x00, 0x89, 0xCA };
                //4号关闭    01 06 00 03 00 01 B8 0A 
                //1 号关闭   01 06 00 00 00 00 89 CA
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                textBox1.Text = "测试串口";
                label2.Text = "打开";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x01, 0x00, 0x01, 0x19, 0xCA };
                //4号关闭    01 06 00 03 00 01 B8 0A 
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                label2.Text = "关闭";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x01, 0x00, 0x00, 0xD8, 0x0A };
                //4号关闭    01 06 00 03 00 01 B8 0A 
                //1 号关闭   01 06 00 00 00 00 89 CA
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                textBox1.Text = "测试串口";
                label3.Text = "打开";

                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x02, 0x00, 0x01, 0xE9, 0xCA };
                //4号关闭    01 06 00 03 00 01 B8 0A      开 01 06 00 02 00 01 E9 CA 
                //4号关闭    01 06 00 03 00 01 B8 0A 
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                textBox1.Text = "测试串口";
                label4.Text = "打开";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                //4号关闭    01 06 00 03 00 01 B8 0A      开 01 06 00 02 00 01 E9 CA 
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                label3.Text = "关闭";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x02, 0x00, 0x00, 0x28, 0x0A };
                //4号关闭    01 06 00 03 00 01 B8 0A 
                //1 号关闭   01 06 00 00 00 00 89 CA
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                label4.Text = "关闭";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x00, 0x79, 0xCA };
                //4号关闭    01 06 00 03 00 01 B8 0A 
                //1 号关闭   01 06 00 00 00 00 89 CA
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                textBox1.Text = "测试串口";
                button9.Text = "已全打开";
                button10.Text = "关闭";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x34, 0x00, 0x01, 0x09, 0xC4 };
                //4号关闭    01 06 00 03 00 01 B8 0A      开 01 06 00 02 00 01 E9 CA 
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            SerialPort MyserialPort = new SerialPort("COM7", 38400, Parity.None, 8, StopBits.One);

            try
            {
                MyserialPort.Open();
                button10.Text = "已全关闭";
                button9.Text = "全部打开";
                // 发送数据 关闭1号  01 06 00 00 00 01 48 0A 
                byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x34, 0x00, 0x00, 0xC8, 0x04 };
                //4号关闭    01 06 00 03 00 01 B8 0A 
                //1 号关闭   01 06 00 00 00 00 89 CA
                //byte[] writeBuffer = { 0x01, 0x06, 0x00, 0x03, 0x00, 0x01, 0xB8, 0x0A };
                MyserialPort.Write(writeBuffer, 0, writeBuffer.Length);

                // 发送完成后，通常需要发送一个RS485的解锁字符
                //MyserialPort.Write(new byte[] { 0x00 }, 0, 1); // 这里的0x00是RS485解锁字符，具体值根据你的设备来定
                byte[] readBuffer = new byte[256];
                int bytesRead = MyserialPort.Read(readBuffer, 0, readBuffer.Length);
                textBox2.Text = bytesRead.ToString();
                // 关闭端口
                MyserialPort.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                textBox1.Text = ex.Message;
            }
        }
    }
}
